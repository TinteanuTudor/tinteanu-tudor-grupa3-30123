import java.util.Scanner;
import java.lang.*;

public class Prime {
    public static int a;
    public static int b;

    public static void Scan() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("introduceti primul numar:");
        a = scanner.nextInt();
        System.out.println("\nintroduceti al doilea numar:");
        b = scanner.nextInt();
    }

    public static boolean isPrime(int x) {
        if (x == 0 || x == 1) return false;
        if (x == 2) return true;
        else {
            for (int i = 3; i <= Math.sqrt(x); i += 2) {
                if (x % i == 0)
                    return false;

            }
        }
        return true;
    }

    public static void main(String[] args) {
        int i;
        int count=0;
        Scan();
        System.out.println(" numerele sunt " + a + " " + b);
        if (a % 2 == 0) {
            for (i = a + 1; i <= b; i += 2) {
                if (isPrime(i))
                    System.out.println(i);
                count++;
            }
        } else {
            for (i = a; i <= b; i += 2) {
                if (isPrime(i))
                    System.out.println(i);
                count++;
            }
        }
        System.out.println("numere prime: " +count);

    }
}

