
import java.util.Scanner;
import java.util.Random;


public class Nr_max_vector {


        public static void main (String[] args)
        {

            Random r = new Random();
            int i,n, max;
            Scanner s = new Scanner(System.in);
            System.out.print("Put the number of elements of the given array:");
            n = s.nextInt();
            int a[] = new int[n];
            System.out.println("Array elements are choosen randomly:");
            for (i = 0; i < n; i++) {
                a[i] = r.nextInt();
                System.out.println(a[i]);
            }
            max = a[0];
            for (i = 0; i < n; i++) {
                if (max < a[i]) {
                    max = a[i];
                }
            }
            System.out.println("Maximum value:" + max);
        }


    }
