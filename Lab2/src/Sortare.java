import java.util.Random;

class BubbleSort {
    void bubbleSort(int arr[]) {
        int n = arr.length;
        for (int i = 0; i < n - 1; i++)
            for (int j = 0; j < n - i - 1; j++)
                if (arr[j] > arr[j + 1]) {
                    // swap arr[j+1] and arr[i]
                    int temp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = temp;
                }
    }

    /* Prints the array */
    void printArray(int arr[]) {
        int n = arr.length;
        for (int i = 0; i < n; ++i)
            System.out.print(arr[i] + " ");
        System.out.println();
    }

    // Driver method to test above
    public static void main(String args[]) {
        int i;
        Random r = new Random();
        BubbleSort ob = new BubbleSort();
        int a[] = new int[10];
        for (i = 0; i < 10; i++) {
             a[i] = r.nextInt();
            System.out.println(a[i]);
        }
        ob.bubbleSort(a);
        System.out.println("Sorted array");
        ob.printArray(a);
    }
}

