import java.util.Scanner;

public class Factorial {
    public static int n;


    public static int fact_recursiv(int n)
    {
        int res;

        if(n==1)
            return 1;

        res = fact_recursiv(n-1) * n;

        return res;

    }

    public static void fact_nerecursiv(int n){
        int result=1;
        for(int i=1;i<=n;i++)
            result*=i;
        System.out.println(result);
    }

    public static void main(String[] args){
        int res;
        Scanner scanner = new Scanner(System.in);
        System.out.println("introduceti  numarul:");
        n = scanner.nextInt();
        System.out.println(fact_recursiv(n));

        fact_nerecursiv(n);

    }

}
