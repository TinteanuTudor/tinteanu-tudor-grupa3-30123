

import java.io.*;

import java.io.Serializable;

public class Car implements Serializable {

    private static final long serialVersionUID = 1L;
    private String model;
    private double price;

    Car() {
    };

    Car(String model, double price) {
       this.model=model;
       this.price=price;
    }

    @Override
    public String toString() {
        return "Car{" + "model='" + model + '\'' + ", price=" + price + '}';
    }
}