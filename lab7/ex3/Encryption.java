
import java.io.*;
import java.util.Scanner;
public class Encryption {
    public void encrypt (File fileName){
        try (Scanner input = new Scanner(fileName)) {
            while (input.hasNextLine()) {
                String line = input.nextLine();
                int[] enc=new int[line.length()];
                for (int i = 0; i < line.length(); i++) {
                    enc[i]=(int) (line.charAt(i))-1;
                    System.out.println(enc[i]);
                }
                try {
                    PrintStream ps = new PrintStream(new File("data_enc.txt"));
                    for (int i = 0; i < line.length(); i++) {
                        ps.println(enc[i]);
                    }
                } catch (IOException e) {
                    System.out.println("Error - " + e.toString());
                }

            }
        } catch (IOException e) {
            System.out.println(e);
        }
    }

}
