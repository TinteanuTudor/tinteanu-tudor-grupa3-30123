

public class TooManyObjectsException extends Exception {

    public TooManyObjectsException(String msg) {
        super(msg);
    }

}
