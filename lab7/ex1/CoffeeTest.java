

public class CoffeeTest {
    public static void main(String[] args) {
        CoffeeMaker mk = new CoffeeMaker();
        CoffeeDrinker d = new CoffeeDrinker();

        for(int i = 0;i<15;i++){
            try {
                Coffee c = mk.makeCoffee();
                d.drinkCofee(c);
            } catch (TemperatureException e) {
                System.out.println("Exception:"+e.getMessage()+" temp="+e.getTemp());
            } catch (ConcentrationException e) {
                System.out.println("Exception:"+e.getMessage()+" conc="+e.getConc());
            } catch (TooManyObjectsException e){
                System.out.println("Exception:"+e.getMessage());
                break;
            }
            finally{
                System.out.println("Throw the cofee cup.\n");
            }

        }
    }
}
