

public class Controller {
    public TemperatureSensor tempSensor = new TemperatureSensor();
    public LightSensor lightSensor = new LightSensor();

    public void control() throws InterruptedException {
        int s = 0;
        while (s <= 20) {
            System.out.println("Temperature: " + tempSensor.readValue() + "\u00B0C");
            System.out.println("Lightning: " + lightSensor.readValue() + " Lumens");
            Thread.sleep(1000);
            s++;
        }
    }
}