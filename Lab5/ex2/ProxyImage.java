

import java.util.Scanner;

public class ProxyImage implements Image {
    Scanner num = new Scanner(System.in);
    private RealImage realImage;
    private String fileName;
    private RotatedImage rotatedImage;

    public ProxyImage(String fileName) {

        this.fileName = fileName;
    }

    @Override
    public void display() {
        System.out.print("Press '0' if you want to display the image or press '1' if you want to rotate the image:  ");
        short opt=num.nextShort();
        switch (opt){
            case 0:{if (realImage == null) {
                realImage = new RealImage(fileName);
            }
                realImage.display();
                break;}
            case 1:{
                if (rotatedImage == null) {
                rotatedImage = new RotatedImage(fileName);
                }
                rotatedImage.display();
                break;
             }
             default:System.out.println("Error");
        }
    }
}