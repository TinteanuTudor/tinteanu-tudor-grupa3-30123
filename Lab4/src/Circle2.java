

public class Circle2 extends Shape {

    private double radius;

    public Circle2() {
        this.radius=1.0;
    }

    public Circle2(double radius) {
        this.radius = radius;
    }

    public Circle2(String color, boolean filled, double radius) {
        super(color, filled);
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double getArea(){
        return Math.PI*getRadius()*getRadius();
    }

    public double getPerimeter(){
        return 2*Math.PI*getRadius();
    }

    @Override
    public String toString() {
        return "A circle with radius "+ getRadius()+ "which is a subclass of " +super.toString();
    }
}
