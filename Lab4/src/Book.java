public class Book {


    private
    String name;
    Author[] author;
    private double price;
    private int qtyInStock;
    private int n=0;
    //constructor with qty

    public Book(String name, Author[] author, double price, int qtyInStock) {
        this.name = name;
        this.author = author;
        this.price = price;
        this.qtyInStock = qtyInStock;
        this.n++;
    }

    // constructor without qty

    public Book(String name, Author[] author, double price) {
        this.name = name;
        this.author = author;
        this.price = price;
        this.n++;
    }




    public String getName() {
        return this.name;
    }

    public Author[] getAuthor() {
        return author;
    }

    public double getPrice() {
        return price;
    }

    public int getQtyInStock() {
        return qtyInStock;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setQtyInStock(int qtyInStock) {
        this.qtyInStock = qtyInStock;
    }

    public String toString(){
        return this.name +"by"+ this.n ;
    }

    public void printAuthors(Author[] author){
        for (int i=0;i<n;i++)
            System.out.println(author[i]);
    }
}
