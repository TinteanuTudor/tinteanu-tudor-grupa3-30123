package aut.utcluj.isp.ex3;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author stefan
 */
public class EquipmentController {

    /**
     * Add new equipment to the list of equipments
     *
     * @param equipment - equipment to be added
     */
    ArrayList<Equipment> echipament = new ArrayList<>();

    public void addEquipment(final Equipment equipment) {
        echipament.add(equipment);
    }

    /**
     * Get current list of equipments
     *
     * @return list of equipments
     */
    public List<Equipment> getEquipments() {
        return echipament;
    }

    /**
     * Get number of equipments
     *
     * @return number of equipments
     */
    public int getNumberOfEquipments() {
        return echipament.size();
    }

    /**
     * Group equipments by owner
     *
     * @return a dictionary where the key is the owner and value is represented by list of equipments he owns
     */
    public Map<String, List<Equipment>> getEquipmentsGroupedByOwner() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    /**
     * Remove a particular equipment from equipments list by serial number
     *
     * @param serialNumber - unique serial number
     * @return deleted equipment instance or null if not found
     */
    public Equipment removeEquipmentBySerialNumber(final String serialNumber) {

        boolean Ok = false;

        for (int i = 0; i < echipament.size(); i++) {

            try {
                if (echipament.get(i).getSerialNumber() == serialNumber) {
                    echipament.remove(i);
                    Ok = true;
                    return echipament.get(i);
                }
            } catch (Exception e) {
            }
        }
        return null;
    }
}
