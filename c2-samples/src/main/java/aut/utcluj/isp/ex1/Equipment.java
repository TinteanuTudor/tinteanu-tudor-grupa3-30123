package aut.utcluj.isp.ex1;

import java.util.Objects;

/**
 * @author stefan
 */
public class Equipment {
    private String name;
    private String serialNumber;



    public Equipment(String serialNumber) {
        this.name = "NONE";
        this.serialNumber=serialNumber;
    }

    public Equipment(String name, String serialNumber) {
        this.name=name;
        this.serialNumber=serialNumber;
    }



    public boolean equals (Object o) {
        if (this == o) return true;
        if (!(o instanceof Equipment)) return false;
        Equipment equipment = (Equipment) o;
        return Objects.equals (getName(),  equipment.getName()) &&
                Objects.equals (getSerialNumber(),  equipment.getSerialNumber());

    }

    public String toString(){
        return name+"_"+serialNumber;
    }

    public String getName() {
        return name;
    }

    public String getSerialNumber() {
        return serialNumber;
    }}



