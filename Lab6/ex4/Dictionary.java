

import java.util.HashMap;
import java.util.Scanner;
public class Dictionary {
    Scanner scanner = new Scanner(System.in);
    private HashMap<Word,Definition> dictionary = new HashMap<>();
    private boolean valid;

    public void addWord( Word w, Definition d) {
        dictionary.put(w, d);
    }
    public Definition getDefinition(Word w){
        return dictionary.get(w);
    }

    public void getAllWords(){
        for (Word word : dictionary.keySet())
            System.out.println(word.getName());
    }

    public void getAllDefinitions() {
        for (Definition def : dictionary.values())
            System.out.println(def.getDescription());
    }

    public void addWordDefinition() {
        String word, description;
        System.out.println("Add word: ");
        word = scanner.next();
        System.out.println("Enter definition for the choosen word: ");
        description = scanner.next();
        Word word_obj = new Word();
        word_obj.setName(word);
        Definition definition = new Definition();
        definition.setDescription(description);
        addWord(word_obj, definition);
    }
    public void readWord() {
        String word;
        valid = false;
        System.out.println("Enter a word :");
        Scanner scanner = new Scanner(System.in);
        word = scanner.nextLine();
        Word w = new Word();
        w.setName(word);
        for (Word i : dictionary.keySet())
            if (i.getName().equals(w.getName())) {
                System.out.println("The definition is:  " + dictionary.get(i).getDescription());
                valid = true;
            }
        if (!valid) System.out.println("Error, inexistent word!!!");
    }
}
