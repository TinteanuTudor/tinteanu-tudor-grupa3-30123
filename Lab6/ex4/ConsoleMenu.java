
import java.util.Scanner;
public class ConsoleMenu {

    public static void main(String[] args) {
        int optiune;
        Scanner scanner = new Scanner(System.in);
        Dictionary dict = new Dictionary();
        System.out.println("\n" +
                " 1.Add word \n" +
                " 2.Get word definition \n" +
                " 3.Get all word from the dictionary" + "\n" +
                " 4.Get all description from the dictionary \n" +
                " 5.Exit \n" +
                " Enter your option: ");

        boolean loop = true;
        while (loop){
            optiune = scanner.nextInt();
            switch (optiune) {
                case 1:
                    dict.addWordDefinition();
                    break;
                case 2:
                    dict.readWord();
                    break;
                case 3:
                    dict.getAllWords();
                    break;
                case 4:
                    dict.getAllDefinitions();
                    break;
                case 5:
                    return;
                default:
                    System.out.println("Optiune inexistenta");
            }
            System.out.println("Enter your option: ");
        }
    }
}