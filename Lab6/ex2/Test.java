

public class Test {
    public static void main(String[] args) {
        Bank bt = new Bank();
        bt.addAccount("Alin", 500);
        bt.addAccount("Andrei", 245);
        bt.addAccount("Ion", 812);
        bt.addAccount("Alex", 351);
        bt.addAccount("Marius", 0);
        bt.printAccounts();
        System.out.println("\n Accounts with balance between 300 and 600 RON: \n");
        bt.printAccounts(300,600);
        System.out.println(bt.getAllAccounts());
    }
}
