
public class Author {
    private String name;
    private String email;
    private char gender;
    public Author(String name,String email,char gender){
        this.name=name;
        this.email=email;
        this.gender=gender;
    }

    public void getName() {
        System.out.println("Name: " + this.name);
    }

    public void getEmail() {
        System.out.println("Email: " + this.email);
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void getGender() {
        System.out.println("Gender: " + this.gender);
    }

    public String toString(){
        return this.name + " ("+ this.gender + ") at " + this.email;
    }

}
