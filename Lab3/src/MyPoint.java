public class MyPoint {
    public int x;
    public int y;
    public MyPoint(){
        this.x=0;
        this.y=0;
    }
    public MyPoint(int x,int y){
        this.x=x;
        this.y=y;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void setXY(int x,int y){
        this.x=x;
        this.y=y;
    }

    public String toString(){
        return "(" + this.x + ", " + this.y + ")";
    }

    public void distance(int x,int y){

        System.out.println("The distance between " + toString() + " and (" + x + "," + y + ") is " + Math.sqrt((this.x-x)*(this.x-x)+(this.y-y)*(this.y-y)));

    }

    public void  distance(MyPoint another){
        System.out.println("The distance between " + toString() + " and (" + another.x + "," + another.y + ") is " + Math.sqrt((this.x-another.x)*(this.x-another.x)+(this.y-another.y)*(this.y-another.y)));
    }

}
